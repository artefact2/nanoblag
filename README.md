nanoblag
========

A tiny static blog engine. Released under the WTFPLv2.

Usage
=====

~~~
$EDITOR md/article1.md
$EDITOR md/article2.md
make
cp out/* /srv/www
~~~

Article template
================

```
    ~~~
    ctime: (latest update time, something parsable by strtotime)
    otime: (creation time, something parsable by strtotime)
    title: (article title)
    lang: (optional, article language subtag)
    ~~~
    (article content, Markdown-formatted...)
```

Thanks
======

* Vollkorn font, released under the OFL (full license text in
  `COPYING.Vollkorn`) <http://vollkorn-typeface.com/>

* Inconsolata font, released under the OFL (full license text in
  `COPYING.Inconsolata`)
  <http://levien.com/type/myfonts/inconsolata.html>

* PHP Markdown Extra, released under 3-clause BSD license (full
  license text in `src/php-markdown/License.md`)
  <https://github.com/michelf/php-markdown>

* bpgdec, released under the BSD license (some parts of ffmpeg under
  LGPL 2.1), see `COPYING.Bpg` <https://bellard.org/bpg/>
