#!/usr/bin/env php
<?php
/* Author: Romain "Artefact2" Dal Maso <artefact2@gmail.com> */

/* This program is free software. It comes without any warranty, to the
 * extent permitted by applicable law. You can redistribute it and/or
 * modify it under the terms of the Do What The Fuck You Want To Public
 * License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

namespace NanoBlag;

require __DIR__.'/common.php';

/* XXX: gitlab doesn't serve .xhtml documents as application/xhtml+xml
 * but as text/xml, and this causes some very hard-to-diagnose issues
 * with canvas and bpgdec8.js. As a crude hack, generate HTML files
 * for now. See:
 * https://gitlab.com/gitlab-org/gitlab-pages/issues/30 */

$root = new XHTMLDocument('..');

$root->head->appendCreate('title', [ 'Image gallery' ]);
$root->head->appendCreate('script', [ 'src' => '../s/bpgdec8.js', '' ]); /* XXX: empty content to force <script></script> for HTML parser */

$root->body->appendCreate('nav');

$hdr = $root->body->appendCreate('header');
$hdr->appendCreate('h1', [ 'Image gallery' ]);

if(!file_exists(__DIR__.'/../static/g')) {
    fprintf(STDERR, "%s: no images, use galaddimg script to add gallery images\n", $argv[0]);
    die(0);
}

if(!file_exists($outdir = __DIR__.'/../out/g')) {
    if(mkdir($outdir) === false) {
        die(1);
    }
}

$ul = $root->body->appendCreate('ul#gallery');

foreach(glob(__DIR__.'/../static/g/*.bpg') as $i) {
    if(preg_match('%^(?<radix>.+)-thumb\.bpg$%', $i, $m)) {
        if(!file_exists($m['radix'].'.bpg')) {
            fatal("loose thumbnail file %s", $i);
        }
        continue;
    }

    $path = pathinfo($i, PATHINFO_DIRNAME);
    $base = pathinfo($i, PATHINFO_FILENAME);
    if(!file_exists($path.'/'.$base.'-thumb.bpg')) {
        fatal("no thumbnail for %s", $i);
    }

    $li = $root->element('li');
    $li->appendCreate('a', [ 'href' => './'.$base.'.html' ])->appendCreate('img', [
        'src' => '../s/g/'.$base.'-thumb.bpg',
        'alt' => $base,
    ]);
    $ul->prepend($li);

    $sub = new XHTMLDocument('..');
    $sub->head->appendCreate('title', [ $base ]);
    $sub->head->appendCreate('script', [ 'src' => '../s/bpgdec8.js', '' ]); /* XXX */
    $sub->head->appendCreate('script', [ 'src' => '../s/gallery.js', '' ]);
    $sub->body->attr('id', 'imgviewer');
    $sub->body->appendCreate('input', [ 'id' => 'img', 'name' => 'img', 'type' => 'checkbox' ]);
    $sub->body->appendCreate('label', [ 'for' => 'img' ])->appendCreate('img', [
        'src' => '../s/g/'.$base.'.bpg',
        'alt' => $base,
    ]);

    if(file_put_contents($outdir.'/'.$base.'.html', "<!DOCTYPE html>\n".$sub->html->renderNode()) === false) {
        die(1);
    }
}


$footer = $root->body->appendCreate('footer');
$p = $footer->appendCreate('p');
$p->append('This image gallery uses ');
$p->appendCreate('a', [ 'href' => 'https://bellard.org/bpg/', 'BPG' ]);
$p->append('. If no images can be seen, enable Javascript and ');
$p->appendCreate('code', [ '<canvas>' ]);
$p->append(' in your browser.');
$frag = $root->createDocumentFragment();
$frag->appendXML(\Michelf\MarkdownExtra::defaultTransform(config('footer_extra')));
$footer->append($frag);

$root->finalize();
die(file_put_contents($outdir.'/index.html', "<!DOCTYPE html>\n".$root->html->renderNode()) === false ? 1 : 0);

