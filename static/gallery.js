"use strict";

const rescroll = e => {
	const img = document.querySelector('canvas, img');
	const html = document.querySelector('html');

	if(img.clientWidth >= img.width) return;

	e.preventDefault();
	e.stopPropagation();

	const cs = getComputedStyle(img);
	const pd = parseFloat(cs.getPropertyValue('border-left-width')) + parseFloat(cs.getPropertyValue('padding-left'));
	const xfrac = (e.clientX - pd - img.offsetLeft) / (img.offsetWidth - 2 * pd);
	const yfrac = (e.clientY - pd - img.offsetTop) / (img.offsetHeight - 2 * pd);

	document.querySelector('input').checked = true;

	html.scrollLeft = img.width * xfrac - e.screenX;
	html.scrollTop = img.height * yfrac - e.screenY;
};

const f = () => {
	const label = document.querySelector('label');
	label.onclick = rescroll;
};

(document.readyState === 'loading') ? document.addEventListener('DOMContentLoaded', f) : f();
