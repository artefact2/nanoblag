MDARTICLES=$(shell find md -name "*.md")
XHARTICLES=$(patsubst md/%.md, out/%.xhtml, $(MDARTICLES))
PAGES=out/index.xhtml $(XHARTICLES)
BPG=$(shell find static -name "*.bpg")

default: $(PAGES) out/s gallery

gallery: $(BPG)
	./src/makegallery

out/index.xhtml: deps.json
	./src/makeindex > $@ || (rm -f $@; exit 1)

out/%.xhtml: deps.json
	./src/makearticle $* > $@ || (rm -f $@; exit 1)

out/s: static
	cp -Tau $< $@

deps.mk deps.json: $(MDARTICLES)
	./src/makedeps

clean:
	rm -f deps.mk deps.json $(PAGES)
	rm -Rf out/s out/g

host:
	@echo "Open http://127.0.0.1:24492/index.xhtml in your browser..."
	php -S 127.0.0.1:24492 -t out

.PHONY: gallery clean host

-include deps.mk
